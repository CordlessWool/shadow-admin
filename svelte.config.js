const { sveltePreprocess } = require("svelte-preprocess/dist/autoProcess");

module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production',
  preprocess: sveltePreprocess({postcss: true})
}