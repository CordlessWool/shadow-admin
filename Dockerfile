FROM node:lts

WORKDIR /app

COPY . .

EXPOSE 5000

CMD ['npm', 'run', 'start']