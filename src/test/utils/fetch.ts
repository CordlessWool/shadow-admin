import { globals } from "svelte/internal";

export function fetchJsonMock(status: number, data: Record<string, unknown>): Partial<Response> {
  return {
    status,
    ok: true,
    json: () => Promise.resolve(data)
  }
}

export function fetchTextMock(status: number, data: string): Partial<Response> {
  return {
    status,
    ok: true,
    text: () => Promise.resolve(data)
  }
}

export function fetchErrorMock(status: number, statusText: string): Partial<Response> {
  return {
    ok: false,
    status,
    statusText
  }
}

export function mockFetch(status: number, returnData: Record<string, unknown> | string): void{
  let response: Partial<Response>;
  if(status >= 400 && typeof returnData === 'string') {
    response = fetchErrorMock(status, returnData);
  } else if(typeof returnData === 'string'){
    response = fetchTextMock(status, returnData);
  } else if(typeof returnData === 'object') {
    response = fetchJsonMock(status, returnData);
  }

  Object.defineProperty(globals, 'fetch', {
    writable: true,
    value: jest.fn().mockImplementation(() => response)
  });
}