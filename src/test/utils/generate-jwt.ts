
import * as jwt from 'jsonwebtoken';
import type { JwtUserData } from '../../schemas/jwt.interface';
import  faker from '@faker-js/faker';

export const generateJwt = (data: JwtUserData, options: jwt.SignOptions = {expiresIn: 1}, secret: string = faker.lorem.word()): string => {
  return jwt.sign(data, secret, options);
};
