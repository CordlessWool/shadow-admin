import { writable } from "svelte/store";
import { User } from "../schemas/user.entity";

export const currentUser = writable<undefined|User>(undefined);