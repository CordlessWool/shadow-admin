import { type Image, type ServerImageList } from "../schemas/image";
import { type NewImage } from "../schemas/image";
import { type Message } from "../schemas/Message.interface";
import { imageServer } from "../utils/server";
import { type dispatchImage } from "./image.store";


const replaceImageId = (currId: string, nextId: string) => (data: Image[]): Image[] => {
  return data.map((img) => {
    if(img.id === currId){
      return {
        ...img,
        id: nextId
      };
    }
    return img;
  });
};

export const add = (...images: Image[]) => (current: Image[]): Image[]  => {
  return [...current, ...images];
};

export const loadImages = async (from: number, take: number): Promise<dispatchImage> => {

  const {data} = await imageServer('IMAGES')
    .get()
    .json<Message<ServerImageList>>();

  const transformed: Image[] = data.map((item) => ({
    id: item.id,
    name: item.name,
    tags: item.tagIds,
    url: item.imageUrl,
    meta: item.dataUrl
  }));

  return add(...transformed);
    
};

export const uploadImage = async ({id, file}: NewImage): Promise<dispatchImage> => {
  const validId = await imageServer('IMAGES')
    .add({
      [file.name]: file
    }, 'form')
    .text();
  
  return replaceImageId(id, validId);

};