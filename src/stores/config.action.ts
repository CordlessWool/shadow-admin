import type { Config } from "../schemas/Config.Interface";

export let config: Config;

export const loadConfig = async (force = false): Promise<Config> => {
  if(!config || force){
    const repsonse = await fetch('/config.json', {
      method: 'GET'
    });
    if (repsonse.ok) {
      config = (await repsonse.json()) as Config;
    } else {
      console.log(repsonse.status, repsonse.statusText);
      throw new Error('failed to load config');
    }
  }

  return config;
};