import { writable } from "svelte/store";
import { Exception } from "../exceptions/exception";
import type { Image, NewImage } from '../schemas/image';
import { noticeStore } from "./notice.store";
export * from './image.actions';

const {set, subscribe, update} = writable<Image[]>([]);

export type dispatchType<T> = (current: T, store?: SvelteStore<T>) => T;
export type dispatchImage = dispatchType<Image[] | NewImage[]>


async function dispatch(promise: Promise<dispatchImage> | dispatchImage) {
  try {
    const fu = await promise;
    update((curr: Image[]) => fu(curr, imageStore));
  } catch (err) {
    if(err instanceof Exception) {
      noticeStore.add(err);
    }
    throw err;
  }
}


export const imageStore = {
  set,
  subscribe,
  dispatch,
};