
import { writable } from "svelte/store";
import { type Notice } from "../schemas/notice.interface";

const {subscribe, update} = writable<Notice[]>([]);

const add = (notice: Notice): void => {
  update((current) => [...current, notice]);
};

function remove (id: Notice['id']): void
function remove (notice: Notice): void
function remove (noticeOrId: Notice | Notice['id']): void {
  update((current: Notice[]) => 
    current.filter((notice) => !(notice === noticeOrId || notice.id === noticeOrId))
  );
}

export const noticeStore = {
  subscribe,
  add,
  remove
};