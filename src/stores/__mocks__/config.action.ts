import type { Config } from "../../schemas/Config.Interface";



export const config: Config = {
  ACCOUNT_API: 'https://account.shadow.photo',
  IMAGE_API: 'https://image.shadow.photo'
};

export const loadConfig = jest.fn().mockImplementation(async (force = false): Promise<Config> => {
  if(force === true){
    return new Promise((resolve) => {
      setTimeout(() => resolve(config), 10);
    });
  }

  return Promise.resolve(config);
});