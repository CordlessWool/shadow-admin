import { Exception } from "./exception";

export enum AuthExceptionTypes {
  JWT_EXPIRED=  "JWT_EXPIRED"
}

export class AuthException extends Exception {

  type: keyof typeof AuthExceptionTypes;

  constructor(type: keyof typeof AuthExceptionTypes){
    super(type);
    this.type = type;

  }
}