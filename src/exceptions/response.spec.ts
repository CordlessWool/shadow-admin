import { ResponseException } from "./response.exception";

describe('test ResponseException', () => {
  it('create Exeption', () => {
    const response = {
      ok: false,
      status: 400,
      statusText: 'Bad Request'
    };
    const exception = new ResponseException(<Response>response);

    expect(exception.message).toBe(response.statusText);
    expect(exception.response).toBeDefined();
    expect(exception.response).toEqual(response);
  });
});