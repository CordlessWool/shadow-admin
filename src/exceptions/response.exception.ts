import { Exception } from "./exception";

export class ResponseException extends Exception {

  response: Response;
  status: number;

  constructor(response: Response){
    super(response.statusText);
    this.response = response;
    this.status = response.status;
  }
}