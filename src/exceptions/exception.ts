import type { Notice } from "../schemas/notice.interface";

export class Exception extends Error implements Notice {
}