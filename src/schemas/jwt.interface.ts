
// eslint-disable-next-line @typescript-eslint/ban-types
export type Jwt<T = {}> = {
  iat: number,
  exp: number,
} & T;

export interface JwtUserData {
  userId: string;
  name: string;
  email: string;
  accountId: string;
}
