import { nanoid } from "nanoid";

export class Tag {
    
  protected _id: string;
  protected _color: string;
  protected _name: string;
  protected _description: string;
  protected _attributes: Record<string, unknown>;
  protected _type: string;

    
  constructor(name: string) {
    this._name = name;
    this._id = nanoid();
    this._color = `#${Math.random().toString(16).slice(2, 9)}`;
    this._type = 'user';
  }

  get id () {
    return this._id;
  }

  set color (colorCode){
    this._color = colorCode;
  }

  get color () {
    return this._color;
  }

  set name (name) {
    this._name = name;
  }

  get name () {
    return this._name;
  }

  set description (description) {
    this._description = description;
  }

  get description () {
    return this._description;
  }

  get attributes () {
    return this._attributes;
  }

  get type () {
    return this._type;
  }

  setAttribute (name, value) {
    if(typeof name === 'string' && name !== 'constructor' && typeof value === 'string'){
      this._attributes[name] = value;
    }
  }
    

    
}