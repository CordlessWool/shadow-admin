import type { Jwt, JwtUserData } from "./jwt.interface";
import { User } from "./user.entity";
import  faker from '@faker-js/faker';
import { generateJwt } from "../test/utils/generate-jwt";
import { currentUser } from "../stores/user.store";
import { get } from "svelte/store";
import { sleep } from "../test/utils/sleep";
import jwtDecode from "jwt-decode";
import { AuthException } from "../exceptions/auth.exception";
import { mockFetch } from "../test/utils/fetch";


jest.mock("../stores/config.action");

describe('test user.class', () => {

  let expired: string;

  beforeAll(() => {
    expired = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI0N2VhODI3Yy0xMTc5LTQ3NjctOWIxZS03MjUwMjEzNDMxNzAiLCJhY2NvdW50SWQiOiJjNDY5Y2U5Ny0zMjRjLTRkY2EtYjI1Ni05M2FiNDI2NWI2ZTciLCJuYW1lIjoiR3JhZHkgTWNEZXJtb3R0IiwiZW1haWwiOiJDb25uZXIuQ3Jpc3Q3N0B5YWhvby5jb20iLCJpYXQiOjE2MzkzMjI0NTUsImV4cCI6MTYzOTMyMjQ1Nn0.ZqC4lcQx7ugW7MDA-Phm8Ym9yd9yUla6pIqQZkNBENk";

  });

  it('shoudl create able', () => {
    const user = new User(faker.internet.email());
    expect(user).toBeInstanceOf(User);
  });


  describe('server communicaiton and interactions', () => {
    let jwtData: JwtUserData;
    let jwt: string;
    beforeEach(() => {
      jwtData = {
        userId: faker.datatype.uuid(),
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
      };
      jwt = generateJwt(jwtData);
    });

    afterEach(() => {
      currentUser.set(undefined);
    });

    it('login', async () => {
      mockFetch(200, jwt);
      
      expect(get(currentUser)).toBeUndefined();

      const spy = jest.spyOn(User.prototype, 'autorenewJwt');

      const user = await User.login(jwtData.email, faker.internet.password());
      expect(spy).toBeCalled();
      spy.mockClear();
      expect(user.id).toBe(jwtData.userId);
      expect(user.accountId).toBe(jwtData.accountId);
      expect(user.name).toBe(jwtData.name);
      expect(user.email).toBe(jwtData.email);

      expect(get(currentUser)).toBeDefined();
    });

    it('logout', async () => {
      mockFetch(200, jwt);

      const user = await User.login(jwtData.email, faker.internet.password());

      expect(get(currentUser)).toBeDefined();

      user.logout();

      expect(user.jwt).toBeUndefined();
      expect(get(currentUser)).toBeUndefined();
      expect(localStorage.getItem('jwt')).toBeNull();

    });

    it('renew jwt', async () => {
      mockFetch(200, jwt);
      

      const user = await User.login(jwtData.email, faker.internet.password());
      const spy = jest.spyOn(user, 'renewJwt');
      expect(user.jwt).toBe(jwt);
      const newJwt = generateJwt(jwtData);
      mockFetch(200, newJwt);

      await sleep(700);
      expect(spy).toBeCalled();
      expect(user.jwt).not.toBe(jwt);
      expect(user.jwt).toBe(newJwt);
    });

    it('do not login if jwt is expired', async () => {
      mockFetch(200, expired);
      expect.assertions(2);
      
      const eData = jwtDecode<Jwt<JwtUserData>>(expired);
      try {
        await User.login(eData.email, expired);
      } catch(err) {
        expect(err).toBeInstanceOf(AuthException);
        expect(get(currentUser)).toBeUndefined();
      }
    });

    it('renew failed of expired jwt', async () => {
      mockFetch(200, jwt);
      

      const user = await User.login(jwtData.email, faker.internet.password());
      const spy = jest.spyOn(user, 'renewJwt');
      mockFetch(401, 'Auth Failed');
      expect(get(currentUser)).toBe(user);

      await user.renewJwt();

      expect(spy).toBeCalled();
      expect(get(currentUser)).toBeUndefined();
    }, 3000);

    it('clear interval of auto renew', async () => {
      mockFetch(200, jwt);

      const user = await User.login(jwtData.email, faker.internet.password());
      const spy = jest.spyOn(user, 'renewJwt');
      spy.mockClear();
      user.stopRenewingJwt();
      await sleep(1000);

      expect(spy).not.toBeCalled();

    });


    const triggerRenewAgain = async (status: number) => {
      mockFetch(200, jwt);

      const user = await User.login(jwtData.email, faker.internet.password());
      const spy = jest.spyOn(user, 'renewJwt');
      mockFetch(status, 'Not Found');
      expect(get(currentUser)).toBe(user);
      user.stopRenewingJwt();
      await user.renewJwt();
      await sleep(1000);

      expect(spy).toBeCalledTimes(2);
      expect(get(currentUser)).toBe(user);
    };

    it('trigger renew again on 404', () => triggerRenewAgain(404));

    it('trigger renew again on 500', async () => triggerRenewAgain(500));

  });

  describe('validation funcitons', () => {
    it('old jwt should be unvalid', () => {      
      expect(User.isJwtValid(expired)).toBe(false);
    });

    it('jwt validation accept decoded jwt', () => {
      const jwtData = jwtDecode<Jwt<JwtUserData>>(expired);
      expect(User.isJwtValid(jwtData)).toBe(false);
    });

    it('new jwt should be valid', () => { 
      const jwtData = {
        userId: faker.datatype.uuid(),
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
      };
      const jwt = generateJwt(jwtData);     
      expect(User.isJwtValid(jwt)).toBe(true);
    });
  });





  
});