export interface Message<T=unknown> {
  data: Array<T>
  total: number,
}