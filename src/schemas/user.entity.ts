import { config } from "../stores/config.action";
import { currentUser } from '../stores/user.store';
import { getQuest, postQuest } from "../utils/request";
import jwtDecode from "jwt-decode";
import type { Jwt, JwtUserData } from "./jwt.interface";
import { AuthException } from "../exceptions/auth.exception";
import { ResponseException } from "../exceptions/response.exception";

enum paths {
  AUTH = '/auth',
  RENEW = '/auth/renew'
}

export class User {

  id: string;

  accountId: string;

  name: string;

  email: string;

  #jwt: string | null | undefined;

  private renewInterval: number | undefined;

  constructor(email: string, jwt?: string | null){
    this.email = email;
    this.#jwt = jwt;

    if(jwt != null){
      localStorage.setItem('jwt', jwt);
      const jwtData = jwtDecode<Jwt<JwtUserData>>(jwt);
      if(!User.isJwtValid(jwt)){
        throw new AuthException('JWT_EXPIRED');
      }
      
      if(jwtData.exp){
        //time in jwt is in seconds, but need ms and reduce time to don't run in
        this.autorenewJwt((jwtData.exp - jwtData.iat) * 700);
      }
      this.id = jwtData.userId;
      this.email = jwtData.email;
      this.name = jwtData.name;
      this.accountId = jwtData.accountId;
    }
  }

  get jwt () {
    return this.#jwt;
  }

  async renewJwt() {
    if(this.#jwt == null) return;
    try {
      this.#jwt = await getQuest({
        base: config.ACCOUNT_API,
        path: paths.RENEW
      }, 'text');
      if(this.#jwt == null){
        this.logout();
      } else {  
        localStorage.setItem('jwt', this.#jwt);
      }
    } catch(err){
      if(err instanceof ResponseException){
        const {status} = err.response; 
        if(status === 404 || status >= 500){
          setTimeout(() => void this.renewJwt(), 1000);
          return;
        } else {
          return this.logout();
        }
      }
      throw err;
    }
  }

  autorenewJwt(time = 5000){
    if(this.#jwt != null) {
      this.renewInterval = window.setInterval(() => {
        void this.renewJwt();
      }, time);
    }
  }

  stopRenewingJwt(){
    clearInterval(this.renewInterval);
  }

  static readJwtFromStorage(): User | undefined {
    const jwt = localStorage.getItem('jwt');
    if(jwt != null){
      const jData = jwtDecode<Jwt<JwtUserData>>(jwt);
      if(!User.isJwtValid(jData)){
        return;
      }
      const user = new User(jData.email, jwt);
      currentUser.set(user);
      void user.renewJwt();
      return user;
    }
  }

  logout() {
    this.#jwt = undefined;
    this.stopRenewingJwt();
    localStorage.clear();
    currentUser.set(undefined);
  }

  static isJwtValid(jwt: string | Jwt) {
    const jwtData = typeof jwt === 'string' ? jwtDecode<Jwt<JwtUserData>>(jwt) : jwt;
    return (jwtData.exp * 1000) > Date.now();
  }

  static async login (email: string, password: string): Promise<User> {
    const jwt = await postQuest({
      base: config.ACCOUNT_API,
      path: paths.AUTH,
      body: {
        username: email,
        password
      }
    });
    
    const user = new User(email, jwt);
    currentUser.set(user);

    return user;
  }
}