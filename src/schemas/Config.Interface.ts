export interface Config {
  ACCOUNT_API: string;
  IMAGE_API: string;
}