import { config } from "../stores/config.action";
import { postQuest } from "../utils/request";
import { ImageMeta } from "./meta.entity";
import { v4 as uuidv4 } from 'uuid';

export class Image {
  id: string;
  url: string;
  #metaId: string;
  meta: ImageMeta;
  saved: boolean;
  

  constructor(id?: string) {
    this.id = id || uuidv4();
  }


  static fromFile(file: File): Image {
    const image = new Image();
    image.url = URL.createObjectURL(file);
    return image;
  }
}