export interface ServerImageList {
  id: string,
  name: string,
  tagIds: Array<string>,
  imageUrl: string,
  dataUrl: string,
}

export interface Image {
  id: string;
  name: string,
  url: string;
  selected: boolean;
}

export interface NewImage extends Image {
  file: File;
  saved: boolean;
}

export enum ImageFitEnum {
  square= 'square',
  aspect= 'aspect'
}

export type ImageFitType = keyof typeof ImageFitEnum;

