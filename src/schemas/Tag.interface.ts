export interface Tag {
    id: string;
    color?: string;
    name: string;
    description?: string;
    attributes?: Record<string, unknown>;
    type?: string;
}