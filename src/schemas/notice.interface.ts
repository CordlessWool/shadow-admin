export enum NoticeTypes {
  success= 'success',
  info= 'info',
  warn= 'warn',
  error= 'error'
}

export interface Notice {
  id?: string | number;
  message: string;
  type?: keyof typeof NoticeTypes,
  deleteAble?: boolean;
}