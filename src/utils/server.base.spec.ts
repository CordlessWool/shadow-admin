import { Server, ServerRequest } from './server.base';
import  faker from '@faker-js/faker';
import { nanoid } from 'nanoid';
import { mockFetch } from '../test/utils/fetch';
import { ResponseException } from '../exceptions/response.exception';

describe('Server Request Base', () => {
  it('create Server with base and path', () => {
    const server = new Server('https://shadow.photo', 'images');
    expect(server).toBeInstanceOf(Server);
  });

  describe('test functions', () => {
    let server: Server;
    let url: URL;
    
    beforeEach(() => {
      url = new URL(faker.internet.url());
      server = new Server(url);
    });

    it('prepare get with auth', () => {
      const token = nanoid();
      const prepared = server.auth(token).get();
    
      expect(prepared).toBeInstanceOf(ServerRequest);
      expect(prepared.request).toEqual({
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: 'GET'
      });
      expect(prepared.url.href).toBe(url.href);
    });

    it('prepare post with auth', () => {
      const data = {
        'shadow': 'ape'
      };
      const token = nanoid();
      const prepared = server.add(data).auth(token);
      
      expect(prepared).toBeInstanceOf(ServerRequest);
      expect(prepared.request).toEqual({
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({
          shadow: 'ape'
        })
      });
      expect(prepared.url.href).toBe(url.href);
    });

    it('set headers and send', () => {
      const data = {
        some: 'shadow'
      };
       
      const prepared = server.header({'x-header': '12345'}).send(data);

      expect(prepared.request).toEqual({
        method: 'POST',
        headers: {
          'x-header': '12345',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
    });

    it('prepare post (json) with params', () => {
      const params = {
        monkey: 'ape',
      };

      const data = {
        some: 'more'
      };

      const prepared = server.params(params).add(data);

      expect(prepared).toBeInstanceOf(ServerRequest);

      const buildUrl = new URL(url);
      buildUrl.searchParams.append('monkey', params.monkey);

      expect(prepared.url.href).toBe(buildUrl.href);

      expect(prepared.request).toEqual({
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });


    });

    it('post formData', () => {
      const formData = new FormData();
      formData.append('name', new Blob());
      formData.append('test', '7');

      const prepared = server.add(formData, true);

      expect(prepared.request).toEqual({
        method: 'POST',
        body: formData,
      });

      expect(prepared.request.body).toBeInstanceOf(FormData);
    });

    it('post: convert json as formData', () => {
      const name = new Blob();
      const formData = new FormData();
      formData.append('name', new Blob());
      formData.append('test', '7');
      const data = {
        name,
        test: 7
      };
      const prepared = server.add(data, 'form');

      expect(prepared.request).toEqual({
        method: 'POST',
        body: formData,
      });

      expect(prepared.request.body).toBeInstanceOf(FormData);
    });

    it('auth with username and password', () => {
      const username = 'some@shadow.photo';
      const password = 'e! -)(jfai';
      const prepared = server.auth(username, password);
      expect(prepared.request).toEqual({
        headers: {
          'Authorization': 'Basic c29tZUBzaGFkb3cucGhvdG86ZSEgLSkoamZhaQ=='
        }
      });
    });

    it('prepare update', () => {
      const id = nanoid();
      const data = {
        name: 'some Name'
      };
      const prepared = server.patch(id, data);
      expect(prepared.url.href).toBe(`${url.href}/${id}`);
      expect(prepared.request).toEqual({
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
    });

    it('prepare update with params', () => {
      const id = nanoid();
      const data = {
        name: 'some Name'
      };
      const prepared = server.patch(id, data).params({
        'update': 7
      });
      expect(prepared.url.href).toBe(`${url.href}/${id}?update=7`);
    });

    it('prepare delete with params', () => {
      const id = nanoid();
      const prepared = server.delete(id).params({
        'all': true
      });
      expect(prepared.url.href).toBe(`${url.href}/${id}?all=true`);
      expect(prepared.request).toEqual({
        method: 'DELETE'
      });
    });

  });


  describe('fetch data', () => {

    let server: Server;
    let url: URL;
    
    beforeEach(() => {
      url = new URL(faker.internet.url());
      server = new Server(url);
    });

    it('execute', async () => {
      mockFetch(200, {tag: 'shadow'});

      const result: Response = await server.get().exec();

      expect(result).toHaveProperty('status');

    });

    it('get json', async () => {
      const res = {tag: 'shadow'};
      mockFetch(200, res);
      const data = await server.get().json();

      expect(data).toEqual(res);
    });

    it('json should throw http error code (400)', async () => {
      mockFetch(400, 'Bad Request');
      expect.assertions(1);

      try {
        await server.get().json();
      } catch (err) {
        expect(err).toBeInstanceOf(ResponseException);
      }
      
    });

    it('json should throw http error code (500)', async () => {
      mockFetch(500, 'Internel Server Error');
      expect.assertions(1);

      try {
        await server.get().json();
      } catch (err) {
        expect(err).toBeInstanceOf(ResponseException);
      }
      
    });

    it('get json', async () => {
      const res = 'monkey';
      mockFetch(200, res);
      const text = await server.get().text();

      expect(text).toBe(res);
    });

    it('text return should throw http error code (400)', async () => {
      mockFetch(400, 'Bad Request');
      expect.assertions(1);

      try {
        await server.get().text();
      } catch (err) {
        expect(err).toBeInstanceOf(ResponseException);
      }
      
    });

    it('text return should throw http error code (500)', async () => {
      mockFetch(500, 'Internel Server Error');
      expect.assertions(1);

      try {
        await server.get().text();
      } catch (err) {
        expect(err).toBeInstanceOf(ResponseException);
      }
      
    });
  });

  
});