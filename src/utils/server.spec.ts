import { imageServer, accountServer, ImagePaths, AccountPaths } from './server';
import { config } from '../stores/config.action';
import { currentUser } from '../stores/user.store';
import { User } from '../schemas/user.entity';

jest.mock('../stores/config.action');

describe('pre prepared Servers', () => {
  it('connection to image', () => {
    const url = new URL(ImagePaths.IMAGES, config.IMAGE_API);
    expect(imageServer('IMAGES').url.href).toBe(url.href);
  });

  it('connection to image with token renew', () => {

    const user = {
      jwt: 'some-jwt'
    };

    currentUser.set(<User>user);

    const url = new URL(ImagePaths.IMAGES, config.IMAGE_API);
    const server = imageServer('IMAGES');
    expect(server.url.href).toBe(url.href);
    expect(server.request.headers).toEqual({
      'Authorization': 'Bearer some-jwt'
    });

    user.jwt = 'some-other-jwt';

    const nServer = imageServer('IMAGES');
    expect(nServer.url.href).toBe(url.href);
    expect(nServer.request.headers).toEqual({
      'Authorization': 'Bearer some-other-jwt'
    });
  });

  it('connection to image with token renew', () => {

    const user = {
      jwt: 'some-jwt'
    };

    currentUser.set(<User>user);

    const url = new URL(ImagePaths.IMAGES, config.IMAGE_API);
    const server = imageServer('IMAGES');
    expect(server.url.href).toBe(url.href);
    expect(server.request.headers).toEqual({
      'Authorization': 'Bearer some-jwt'
    });

    const updatedUser = {
      jwt: 'some-other-jwt'
    };

    currentUser.set(<User>updatedUser);

    const nServer = imageServer('IMAGES');
    expect(nServer.url.href).toBe(url.href);
    expect(nServer.request.headers).toEqual({
      'Authorization': 'Bearer some-other-jwt'
    });
  });

  it('connection to image', () => {
    const url = new URL(AccountPaths.RENEW, config.ACCOUNT_API);
    expect(accountServer('RENEW').url.href).toBe(url.href);
  });

  it('connection to account with token renew', () => {

    const user = {
      jwt: 'some-jwt'
    };

    currentUser.set(<User>user);

    const server = accountServer('RENEW');
    expect(server.request.headers).toEqual({
      'Authorization': 'Bearer some-jwt'
    });

    user.jwt = 'some-other-jwt';

    const nServer = accountServer('AUTH');
    expect(nServer.request.headers).toEqual({
      'Authorization': 'Bearer some-other-jwt'
    });
  });
});