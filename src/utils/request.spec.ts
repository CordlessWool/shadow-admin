import { createUrl, getQuest, type GetRequest, postQuest } from "./request";
import  faker from '@faker-js/faker';
import { globals } from "svelte/internal";
import { fetchErrorMock, fetchJsonMock, fetchTextMock } from "../test/utils/fetch";
import { ResponseException } from "../exceptions/response.exception";

describe('request spec', () => {
  describe('build url', () => {
    it('base with path', () => {
      const params = {
        base: 'http://test.shadow.photo',
        path: '/auth'
      };
  
      const url = createUrl(params);

      expect(url.href).toBe('http://test.shadow.photo/auth');
    });

    it('base without slash', () => {
      const params = {
        base: 'http://test.shadow.photo',
        path: 'auth'
      };
  
      const url = createUrl(params);

      expect(url.href).toBe('http://test.shadow.photo/auth');
    });


    it('base with query', () => {
      const params = {
        base: 'http://test.shadow.photo',
        path: 'image',
        query: {
          id: faker.datatype.uuid(),
          height: 3000
        }
      };
  
      const url = createUrl(params);

      expect(url.href).toBe(`http://test.shadow.photo/image?id=${params.query.id}&height=${params.query.height}`);
    });

    it('encoded url in query', () => {
      const params = {
        base: 'http://test.shadow.photo',
        path: 'image',
        query: {
          url: 'https://image.shadow.photo?test=! !!?"'
        }
      };
  
      const url = createUrl(params);

      expect(url.href).toBe('http://test.shadow.photo/image?url=https%3A%2F%2Fimage.shadow.photo%3Ftest%3D%21+%21%21%3F%22');
    });
    
    
  });

  describe('test getQuest', () => {

    let baseRequestObject: GetRequest;

    beforeEach(() => {
      baseRequestObject = {
        base: 'http://test.de',
        path: 'path'
      };
    });

    afterEach(() => {
      (globals.fetch as jest.Mock).mockClear();
    });

    it('request json', async () => {
      Object.defineProperty(globals, 'fetch', {
        writable: true,
        value: jest.fn().mockImplementation(() => fetchJsonMock(201, {
          name: 'test'
        }))
      });
      const data = await getQuest<{name: string}>(baseRequestObject);
      expect(data).toEqual({
        name: 'test'
      });
    });

    it('request string', async () => {
      const value = faker.lorem.words();
      Object.defineProperty(globals, 'fetch', {
        writable: true,
        value: jest.fn().mockImplementation(() => fetchTextMock(201, value))
      });
      const data = await getQuest(
        baseRequestObject,
        'text'
      );
      expect(data).toBe(value);
    });

    it('throw error', async () => {
      expect.assertions(2);
      const errorText = 'Not Authenticated';

      Object.defineProperty(globals, 'fetch', {
        writable: true,
        value: jest.fn().mockImplementation(() => fetchErrorMock(401, errorText))
      });
      
      try {
        await getQuest(
          baseRequestObject,
          'text'
        );
      } catch(err) {
        expect(err).toBeInstanceOf(ResponseException);
        if (err instanceof ResponseException) {
          expect(err.response).toEqual({
            ok: false,
            status: 401,
            statusText: errorText
          });
        }
      }
  
      
    });
  });


  describe('test postQuest', () => {

    let baseRequestObject: GetRequest;

    beforeEach(() => {
      baseRequestObject = {
        base: 'http://test.de',
        path: 'path'
      };
    });

    afterEach(() => {
      (globals.fetch as jest.Mock).mockClear();
    });

    it('request', async () => {
      const value = faker.lorem.words();
      Object.defineProperty(globals, 'fetch', {
        writable: true,
        value: jest.fn().mockImplementation(() => fetchTextMock(201, value))
      });
      const data = await postQuest(
        baseRequestObject
      );
      expect(data).toBe(value);
    });

    it('throw error', async () => {
      expect.assertions(2);
      const errorText = 'Bad Request';
      Object.defineProperty(globals, 'fetch', {
        writable: true,
        value: jest.fn().mockImplementation(() => fetchErrorMock(400, errorText))
      });
      try {
        await postQuest(baseRequestObject);
      } catch(err) {
        expect(err).toBeInstanceOf(ResponseException);
        if (err instanceof ResponseException) {
          expect(err.response).toEqual({
            ok: false,
            status: 400,
            statusText: errorText
          });
        }
      }
    });
  });
});