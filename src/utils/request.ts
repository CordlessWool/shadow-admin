import { currentUser } from "../stores/user.store";
import { get as getStoreContent } from 'svelte/store';
import { type Message } from "../schemas/Message.interface";
import { ResponseException } from "../exceptions/response.exception";

type returnTypeOptions = 'text' | 'json';

export type GetRequest = {
  base: string,
  path: string,
  headers?: HeadersInit,
  query?: Record<string, string | number>,
}

export type PostRequest = GetRequest & {
  body?: Record<string, unknown>
}

type responseObject<T = unknown> = T | Message<T> | null;
type responseString = string | null;
type responseType<T> = responseObject<T> | responseString

function handleResult(response: Response, returnType: 'text'): Promise<responseString>
function handleResult<T extends Record<string, unknown>> (response: Response, returnType: 'json'): Promise<responseType<T>>
function handleResult<T> (response: Response, returnType: returnTypeOptions): Promise<responseType<T>>
function handleResult<T> (response: Response, returnType: returnTypeOptions): Promise<responseType<T>> {
  if(!response.ok) {
    throw new ResponseException(response);
  }

  if(response.status === 204){
    return Promise.resolve(null);
  }

  switch(returnType) {
  case 'text': return response.text();
  case 'json': return response.json() as Promise<Message<T>>;
  }
}

const getAuthHeader = (): Record<string, string> => {
  const user = getStoreContent(currentUser);
  if(user?.jwt != null) {
    return {
      Authorization: `Bearer ${user.jwt}`
    };
  } 
  return {};
};


export const createUrl = ({base, path, query}: Pick<GetRequest, 'base' | 'path' | 'query'>) => {
  const url = new URL(path, base);
  if(query != null && typeof query === 'object'){
    Object.entries(query).forEach(
      ([k, v]) => url.searchParams.append(k, v.toString())
    );
  } 

  return url;
};

/**
 * Do an fetch request to the server and return a string or object
 * 
 * @param {GetRequest} data 
 */
export async function getQuest (data : GetRequest, responseType: 'text') : Promise<responseString>
export async function getQuest<T = Record<string, unknown>> (data : GetRequest, responseType?: 'json') : Promise<T | null>
//export async function getQuest<T = unknown> (data : GetRequest) : Promise<responseType<T>>
export async function getQuest<T = Record<string, unknown>> ({
  base, path, headers, query
}: GetRequest, responseType: returnTypeOptions = 'json') : Promise<responseType<T>> {
  const url = createUrl({base, path, query});
  const response = await fetch(url.href, {
    method: 'get',
    headers: { 
      'Content-Type': 'application/json',
      ...getAuthHeader(),
      ...headers
    }
  });

  return handleResult<T>(response, responseType);
}



export const postQuest = async ({ base, path, body, headers, query }: PostRequest): Promise<responseString> => {
  const url = createUrl({base, path, query});
  const response = await fetch(url.href, {
    method: 'post',
    headers: { 
      'Content-Type': 'application/json',
      ...getAuthHeader(),
      ...headers
    },
    body: JSON.stringify(body)
  });

  return handleResult(response, 'text');  
};