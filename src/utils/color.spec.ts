import {getContrastColorBlackWhite} from './color'

describe('Contrast Color', () => {
  it('background: white', () => {
    const contrast = getContrastColorBlackWhite('#FFF');
    expect(contrast).toBe('black');
  })

  it('background: black', () => {
    const contrast = getContrastColorBlackWhite('#000');
    expect(contrast).toBe('white');
  })

  it('background: #61788E', () => {
    const contrast = getContrastColorBlackWhite('#61788E');
    expect(contrast).toBe('white');
  })

  it('background: #6F8F61', () => {
    const contrast = getContrastColorBlackWhite('#6F8F61');
    expect(contrast).toBe('white');
  })

  it('background: #4FAC27', () => {
    const contrast = getContrastColorBlackWhite('#4FAC27');
    expect(contrast).toBe('black');
  })

  it('background: #AB2727', () => {
    const contrast = getContrastColorBlackWhite('#61788E');
    expect(contrast).toBe('white');
  })


  it('background: #DBAAAA', () => {
    const contrast = getContrastColorBlackWhite('#DBAAAA');
    expect(contrast).toBe('black');
  })


})