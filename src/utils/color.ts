export const getContrastColorBlackWhite = (color: string): 'black'|'white' => {
  let green, blue, red;
  if(typeof color === 'string'){
    const hex = [...color];
    if(hex.length === 4){
      red = "0x"+hex[1]+hex[1];
      green = "0x"+hex[2]+hex[2];
      blue = "0x"+hex[3]+hex[3];   
    } else {
      red = "0x"+hex[1]+hex[2];
      green = "0x"+hex[3]+hex[4];
      blue = "0x"+hex[5]+hex[6];
    }
  }
  const sum = Math.round(((red * 299) + (green * 587) + (blue * 114)) / 1000);
  return (sum > 128) ? 'black' : 'white';
};

export const defaultColorCodes: Array<string> = ['#00CED1', '#0000FF', '#4169E1', '#F8F8FF', '#008000', '#32CD32', '#FFD700', '#F08080'];

export const getRandomColor = (): string => {
  const random = Number((Math.random() * defaultColorCodes.length).toFixed(0));
  return defaultColorCodes[random];
};