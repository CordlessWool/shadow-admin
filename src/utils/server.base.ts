import { ResponseException } from "../exceptions/response.exception";

export type toStringFunction = () => string;
export type toString = {toString: toStringFunction};
export type QueryData = Record<string, toString>;
export type FormDataObject = Record<string, toString | Blob>

export class Server {

  request: RequestInit;
  url: URL;

  constructor(url: URL)
  constructor(base: string, path: string)
  constructor(baseOrUrl: string | URL, path?: string) {
    if (path != null) {
      this.url = new URL(path, baseOrUrl);
    } else {
      this.url = new URL(baseOrUrl);
    }

    this.request = {};
  }

  get(): ServerRequest {
    this.request.method = 'GET';
    return new ServerRequest(this);
  }

  protected transformToFormData(data: FormDataObject): [FormData, boolean]{
    let multipart = false;
    const formData = new FormData();
    Object
      .entries(data)
      .forEach(
        ([k, v]) => {
          let value: string | Blob;
          if(v instanceof Blob){
            value = v;
            multipart = true;
          } else {
            value = v.toString();
          }
          formData.append(k, value);
        }
      );
    return [formData, multipart];
  }

  protected extendUrlPath(path: string | number) {
    this.url.pathname += `/${path}`;
  }

  add(data: Record<string, unknown>): ServerRequest
  add(data: FormData, multipart?: boolean): ServerRequest
  add(data: FormDataObject, type: 'form'): ServerRequest
  add(data: Record<string, unknown>, type: 'json'): ServerRequest
  add(data: Record<string, unknown> | FormData, type?: 'form' | 'json' | boolean): ServerRequest{
    this.request.method = 'POST';
    
    if(type === 'form'){
      
      const [formData, multipart] = this.transformToFormData(<FormDataObject>data);
      this.request.body = formData;
      this.setFormHeader(multipart);
    } else if (data instanceof FormData) {
      this.request.body = data;
      this.setFormHeader(!!type);
    } else {
      this.request.body = JSON.stringify(data);
      this.setJsonHeader();
    }

    
    return new ServerRequest(this);
  }

  send(data: Record<string, unknown>): ServerRequest{
    return this.add(data);
  }

  patch(id: string | number, data: Record<string, unknown>): ServerRequest{
    this.extendUrlPath(id);
    this.request.method = 'PATCH';
    this.request.body = JSON.stringify(data);
    this.setJsonHeader();

    return new ServerRequest(this);
  }

  delete(id: string | number): ServerRequest{ 
    this.extendUrlPath(id);
    this.request.method = 'DELETE';

    return new ServerRequest(this);
  }

  params(options: QueryData): this {
    Object.entries(options).forEach(
      ([k, v]) => this.url.searchParams.append(k, v.toString())
    );

    return this;
  }

  protected setJsonHeader() {
    this.header('Content-Type', 'application/json');
  }

  protected setFormHeader(multpart = false) {
    if(!multpart){
      const value = 'application/ x-www-form-urlencoded';
      this.header('Content-Type', value);
    }
  }

  header(object: Record<string, string>): this
  header(name: string, value: toString): this
  header(nameOrObject: string | Record<string, string>, value?: toString): this {
    if(typeof nameOrObject === 'string'){
      const str = value?.toString() || "";
      this.request.headers = {
        ...this.request.headers,
        [nameOrObject]: str,
      };
    } else {
      this.request.headers = {
        ...this.request.headers,
        ...nameOrObject
      };
    }
    return this;
  }


  auth(token: string): this
  auth(username: string, password: string): this
  auth(usernameOrToken: string, password?: string): this{
    let value: string;
    if(password == null){
      value = `Bearer ${usernameOrToken}`;
    } else {
      const encoded = window.btoa(`${usernameOrToken}:${password}`);
      value = `Basic ${encoded}`;
    }
    this.header('Authorization', value);

    return this;
  }
}

export class ServerRequest extends Server {

  protected response: Response;

  constructor(server: Server) {
    super(server.url);
    this.request = server.request;
  }

  protected handleError(res: Response){
    if(res.status >= 400){
      throw new ResponseException(res);
    }
  }

  async text(): Promise<string> {
    const response = await this.exec();

    this.handleError(response);

    return response.text();
  }

  async json<T>(): Promise<T> {
    const response = await this.exec();

    this.handleError(response);
    
    return <T> await response.json();
  }

  async exec(): Promise<Response> {
    this.response = await fetch(this.url.href, this.request);

    return this.response;
  }
}