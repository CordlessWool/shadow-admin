import { Server } from "./server.base";
import { config } from "../stores/config.action";
import { currentUser } from "../stores/user.store";
import { User } from "../schemas/user.entity";
import { get } from "svelte/store";

export enum AccountPaths {
  AUTH = '/auth',
  RENEW = '/auth/renew'
}

export enum ImagePaths {
  IMAGES = 'images'
}

// let token: string | null | undefined;

// currentUser.subscribe((user) => token = user?.jwt);

export const imageServer = (path: keyof typeof ImagePaths): Server => {
  const server =  new Server(config.IMAGE_API, ImagePaths[path]);

  const user: User | undefined = get(currentUser);
  if(user != null && user.jwt != null){
    server.auth(user.jwt);
  }

  return server;
};

export const accountServer = (path: keyof typeof AccountPaths): Server => {
  const server = new Server(config.ACCOUNT_API, AccountPaths[path]);

  const user: User | undefined = get(currentUser);
  if(user != null && user.jwt != null){
    server.auth(user.jwt);
  }

  return server;
};