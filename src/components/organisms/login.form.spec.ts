import { fireEvent } from '@testing-library/dom';
import '@testing-library/jest-dom';
import { render } from '@testing-library/svelte';
import { get } from 'svelte/store';
import { User } from '../../schemas/user.entity';
import { currentUser } from '../../stores/user.store';
import LoginForm from './login.form.svelte';
import  faker from '@faker-js/faker';

describe('test login form', () => {
  test('render all components', () => {
    const {getByText, getByLabelText} = render(LoginForm);
    expect(getByText('Sign In')).toBeInTheDocument();
    expect(getByLabelText('E-Mail')).toBeInTheDocument();
    expect(getByLabelText('Password')).toBeInTheDocument();
  });

  test('Buttton should equal input size', () => {
    const {getByText, getByLabelText} = render(LoginForm);
    const button = getByText('Sign In');
    const input = getByLabelText('Password');
    const ih = input.offsetHeight;
    const bh = button.offsetHeight;
    expect(bh).toBe(ih);

    const iw = input.offsetWidth;
    const bw = button.offsetWidth;
    expect(bw).toBe(iw);
  });

  test('submit an call login', () => {
    const email= faker.internet.email();
    render(LoginForm, {
      email,
      password: 'secure'
    });

    jest.spyOn(User, 'login').mockImplementationOnce((email: string, _password: string) => {
      const user = new User(email);
      currentUser.set(user);
      return Promise.resolve(user);
    });
    const button = document.querySelector('button[type="submit"]');
    if(button == null) throw new Error('submit button is required');
    fireEvent.click(button);

    expect(get(currentUser)?.email).toBe(email);

  });

});