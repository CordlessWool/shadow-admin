import '@testing-library/jest-dom';
import { render } from "@testing-library/svelte";
import { nanoid } from "nanoid";
import faker from '@faker-js/faker';
import Input from "./input.svelte";

describe('input component', () => {
  test('should have id', () => {
    const id = nanoid(3);
    render(Input, 
      { 
        props: {
          value: "test",
          placeholder: 'place',
          id
        }
      }
    );

    expect(document.querySelector(`#input-${id}`)).toBeInTheDocument();
  });

  test('should render', () => {
    const id = '777';
    const comp = render(Input, 
      { 
        props: {
          value: "test",
          placeholder: 'place',
          id
        }
      }
    );

    expect(comp.getByPlaceholderText('place')).toBeInTheDocument();
  });

  test('set Value', () => {
    const value = faker.lorem.word();
    const comp = render(Input, 
      { 
        props: {
          value,
          placeholder: 'place',
          id: faker.datatype.number()
        }
      }
    );

    expect(comp.getByDisplayValue(value)).toBeInTheDocument();
  });
});