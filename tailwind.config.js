module.exports = {
  content: [
    './src/**/*.svelte',
    './src/**/*.js',
    './src/**/*.html'
  ],
  //darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {
      borderColor: ['active']
    },
  },
  plugins: [
  ],
}
